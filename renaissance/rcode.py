# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship

import web
from web import ctx
import model as gmodel
import renaissance.rmodel as rmodel
import renaissance.rview as rview
from renaissance.rview import render


def readandupdatewounds(inp,perso):
    bps = 0
    bpl = 0
    bpg = 0
    bms = 0
    bml = 0
    bmg = 0
    resp = 0        
    pid = perso["id"]
    #Blessures Physiques Superficielles
    for x in range(perso["bpsM"]):
        if hasattr(inp,"bps"+str(pid)+"B"+str(x)) and inp["bps"+str(pid)+"B"+str(x)]:
            bps+=1
    #Blessures Physiques Légères
    for x in range(perso["bplM"]):
        if hasattr(inp,"bpl"+str(pid)+"B"+str(x)) and inp["bpl"+str(pid)+"B"+str(x)]:
            bpl+=1
    #Blessures Physiques Graves
    for x in range(perso["bpgM"]):
        if hasattr(inp,"bpg"+str(pid)+"B"+str(x)) and inp["bpg"+str(pid)+"B"+str(x)]:
            bpg+=1
    #Blessures Mentales Superficielles
    for x in range(perso["bmsM"]):
        if hasattr(inp,"bms"+str(pid)+"B"+str(x)) and inp["bms"+str(pid)+"B"+str(x)]:
            bms+=1
    #Blessures Mentales Légères
    for x in range(perso["bmlM"]):
        if hasattr(inp,"bml"+str(pid)+"B"+str(x)) and inp["bml"+str(pid)+"B"+str(x)]:
            bml+=1
    #Blessures Mentales Graves
    for x in range(perso["bmgM"]):
        if hasattr(inp,"bmg"+str(pid)+"B"+str(x)) and inp["bmg"+str(pid)+"B"+str(x)]:
            bmg+=1
    #Resplioide
    for x in range(perso["resplioideM"]):
        if hasattr(inp,"resplioide"+str(pid)+"B"+str(x)) and inp["resplioide"+str(pid)+"B"+str(x)]:
            resp+=1
    perso["bps"] = bps
    perso["bpl"] = bpl
    perso["bpg"] = bpg
    perso["bms"] = bms
    perso["bml"] = bml
    perso["bmg"] = bmg
    perso["resplioideb"] = resp
    rmodel.updatewounds(pid,bps,bpl,bpg,bms,bml,bmg,resp)
    calctemprunes(perso)

def calctemprunes(perso):
    pid = perso["id"]
    malus = 10*(perso["bmg"]+perso["bpg"])
    imm = perso["immersionP"] - perso["immersionN"]
    #Vie
    tvie = perso["viebase"] + imm
    if tvie <= 0:
        tvie = 0
    else:
        tvie = max(1,tvie-malus)
    #Mort
    tmort = perso["mortbase"] + imm
    if tmort <= 0:
        tmort = 0
    else:
        tmort = max(1,tmort-malus)
    #Cosmo
    tcosmo = perso["cosmobase"] + imm
    if tcosmo <= 0:
        tcosmo = 0
    else:
        tcosmo = max(1,tcosmo-malus)
    #Chaos
    tchaos = perso["chaosbase"] + imm
    if tchaos <= 0:
        tchaos = 0
    else:
        tchaos = max(1,tchaos-malus)
    #Creation
    tcreation = perso["creationbase"] + imm
    if tcreation <= 0:
        tcreation = 0
    else:
        tcreation = max(1,tcreation-malus)
    #Neant
    tneant = perso["neantbase"] + imm
    if tneant <= 0:
        tneant = 0
    else:
        tneant = max(1,tneant-malus)
    perso["vievalue"] = tvie
    perso["mortvalue"] = tmort
    perso["cosmovalue"] = tcosmo
    perso["chaosvalue"] = tchaos
    perso["creationvalue"] = tcreation
    perso["neantvalue"] = tneant    
    rmodel.updatetemprunes(pid,tvie,tmort,tcosmo,tchaos,tcreation,tneant)

def readandupdatepoints(inp,perso):
    pointsr = perso["points"]
    if hasattr(inp,"points"):
        pointsr = int(inp["points"])
        perso["points"] = pointsr
    pointsl = perso["liensp"]
    oldpoints = pointsl
    if hasattr(inp,"liens"):
        pointsl = int(inp["liens"])
        perso["liensp"] = pointsl
        perso["lienspr"] = perso["lienspr"]+(pointsl-oldpoints)
    rmodel.updatepoints(perso["id"],pointsr,pointsl)

def readandupdateoffsets(inp,perso):
    vie = perso["vieoffset"]
    mort = perso["mortoffset"]
    cosmo = perso["cosmooffset"]
    chaos = perso["chaosoffset"]
    creation = perso["creationoffset"]
    neant = perso["neantoffset"]
    if hasattr(inp,"vieoffset"):
        vie = int(inp["vieoffset"])
    if hasattr(inp,"mortoffset"):
        mort = int(inp["mortoffset"])
    if hasattr(inp,"cosmooffset"):
        cosmo = int(inp["cosmooffset"])
    if hasattr(inp,"chaosoffset"):
        chaos = int(inp["chaosoffset"])
    if hasattr(inp,"creationoffset"):
        creation = int(inp["creationoffset"])
    if hasattr(inp,"neantoffset"):
        neant = int(inp["neantoffset"])
    perso["vieoffset"] = vie
    perso["mortoffset"] = mort
    perso["cosmooffset"] = cosmo
    perso["chaosoffset"] = chaos
    perso["creationoffset"] = creation
    perso["neantoffset"] = neant
    rmodel.updateoffsets(perso["id"],vie,mort,cosmo,chaos,creation,neant)
    

def reandandupdateimmersion(inp,perso):
    immP = perso["immersionP"]
    if hasattr(inp,"immersionP"):
        immP = int(inp["immersionP"])
        perso["immersionP"] = immP
    immN = perso["immersionN"]
    if hasattr(inp,"immersionN"):
        immN = int(inp["immersionN"])
        perso["immersionN"] = immN
    rmodel.updateimmersion(perso["id"],immP,immN)

def reandandupdateresplioidebless(inp,perso):
    bless = perso["resplioideD"]
    if hasattr(inp,"respd"):
        bless = int(inp["respd"])
        perso["resplioideD"] = bless
    arm = perso["resplioideM"]
    if hasattr(inp,"respd"):
        arm = int(inp["respb"])
        perso["resplioideM"] = arm
    rmodel.updateresplioidebless(perso["id"],bless,arm)

def reandandupdatefaitsrunes(inp,perso):
    for fait in perso["faits"]:
        vie = fait["vie"]
        mort = fait["mort"]
        cosmo = fait["cosmo"]
        chaos = fait["chaos"]
        creation = fait["creation"]
        neant = fait["neant"]
        if hasattr(inp,"vie"+str(fait["id"])):
            vie = int(inp["vie"+str(fait["id"])])
        if hasattr(inp,"mort"+str(fait["id"])):
            mort = int(inp["mort"+str(fait["id"])])
        if hasattr(inp,"cosmo"+str(fait["id"])):
            cosmo = int(inp["cosmo"+str(fait["id"])])
        if hasattr(inp,"chaos"+str(fait["id"])):
            chaos = int(inp["chaos"+str(fait["id"])])
        if hasattr(inp,"creation"+str(fait["id"])):
            creation = int(inp["creation"+str(fait["id"])])
        if hasattr(inp,"neant"+str(fait["id"])):
            neant = int(inp["neant"+str(fait["id"])])
        fait["vie"] = vie
        fait["mort"] = mort
        fait["cosmo"] = cosmo
        fait["chaos"] = chaos
        fait["creation"] = creation
        fait["neant"] = neant
        rmodel.updatefaitrunes(fait["id"],vie,mort,cosmo,chaos,creation,neant)

def updaterunes(perso):
    cap = 100+(perso["liensp"] - perso["lienspr"])
    vie_coef = 0
    mort_coef = 0
    cosmo_coef = 0
    chaos_coef = 0
    neant_coef = 0
    creation_coef = 0
    vie = 0
    mort = 0
    cosmo = 0
    chaos = 0
    neant = 0
    creation = 0
    for fait in perso["faits"]:
        vie_coef += int(fait["vie"])
        mort_coef += int(fait["mort"])
        cosmo_coef += int(fait["cosmo"])
        chaos_coef += int(fait["chaos"])
        neant_coef += int(fait["neant"])
        creation_coef += int(fait["creation"])
    vie_coef = max(vie_coef,1)
    mort_coef = max(mort_coef,1)
    cosmo_coef = max(cosmo_coef,1)
    chaos_coef = max(chaos_coef,1)
    neant_coef = max(neant_coef,1)
    total = max(vie_coef+mort_coef+cosmo_coef+chaos_coef+neant_coef+creation_coef,1)
    
    vie = max(int((float(vie_coef)/float(total))*perso["points"])+perso["vieoffset"],1)
    mort = max(int((float(mort_coef)/float(total))*perso["points"])+perso["mortoffset"],1)
    cosmo = max(int((float(cosmo_coef)/float(total))*perso["points"])+perso["cosmooffset"],1)
    chaos = max(int((float(chaos_coef)/float(total))*perso["points"])+perso["chaosoffset"],1)
    creation = max(int((float(creation_coef)/float(total))*perso["points"])+perso["creationoffset"],1)
    neant = max(int((float(neant_coef)/float(total))*perso["points"])+perso["neantoffset"],1)

    #cap
    capVM = False
    pVM = vie+mort
    capCC = False
    pCC = cosmo+chaos
    capCN = False
    pCN = creation+neant
    if pVM >= cap:
        capVM = True
        if pVM > cap:
            pVM = cap
            if vie > mort:
                if vie >= cap:
                    vie = cap-1
                    mort = 1
                else:
                    mort = cap-vie
            else:
                if mort >= cap:
                    mort = cap-1
                    vie = 1
                else:
                    vie = cap-mort
    if pCC >= cap:
        capCC = True
        if pCC > cap:
            pCC = cap
            if cosmo > chaos:
                if cosmo >= cap:
                    cosmo = cap-1
                    chaos = 1
                else:
                    chaos = cap-cosmo
            else:
                if chaos >= cap:
                    chaos = cap-1
                    cosmo = 1
                else:
                    cosmo = cap-chaos
    if pCN >= cap:
        capCN = True
        if pCN > cap:
            pCN = cap
            if creation > neant:
                if creation >= cap:
                    creation = cap-1
                    neant = 1
                else:
                    neant = cap-creation
            else:
                if neant >= cap:
                    neant = cap-1
                    creation = 1
                else:
                    creation = cap-neant
    #Répartition du reste des points
    reste = perso["points"]+perso["vieoffset"]+perso["mortoffset"]+perso["cosmooffset"]+perso["chaosoffset"]+perso["creationoffset"]+perso["neantoffset"]-pVM-pCC-pCN
    
    while(reste !=0):
        dic = {"vie":vie,"mort":mort,"cosmo":cosmo,"chaos":chaos,"creation":creation,"neant":neant}
        runes = sorted(dic,key=dic.get,reverse=True)
        for r in runes:
            if r == "vie" and not capVM:
                d = min(reste,cap-pVM)
                vie +=d
                reste-=d
                pVM +=d
                if pVM == cap:
                    capVM = True
            elif r  == "mort" and not capVM:
                d = min(reste,cap-pVM)
                mort +=d
                reste-=d
                pVM +=d
                if pVM == cap:
                    capVM = True
            elif r  == "cosmo" and not capCC:
                d = min(reste,cap-pCC)
                cosmo +=d
                reste-=d
                pCC +=d
                if pCC == cap:
                    capCC = True
            elif r  == "chaos" and not capCC:
                d = min(reste,cap-pCC)
                chaos +=d
                reste-=d
                pCC +=d
                if pCC == cap:
                    capCC = True
            elif r  == "creation" and not capCN:
                d = min(reste,cap-pCN)
                creation +=d
                reste-=d
                pCN +=d
                if pCN == cap:
                    capCN = True
            elif r  == "neant" and not capCN:
                d = min(reste,cap-pCN)
                neant +=d
                reste-=d
                pCN +=d
                if pCN == cap:
                    capCN = True
        if reste != 0:
            print("(calculate_runes) full cap reached before reste = 0")
            reste = 0
    perso["viebase"] = vie
    perso["mortbase"] = mort
    perso["cosmobase"] = cosmo
    perso["chaosbase"] = chaos
    perso["creationbase"] = creation
    perso["neantbase"] = neant
    #Update op
    if perso["resplioideM"] > 0:
        opM = creation
    else:
        opM = vie
    perso["opM"] = opM
    #Update blessures
    bpsM = 1
    bplM = 1
    bpgM = 1
    bmsM = 1
    bmlM = 1
    bmgM = 1
    if vie > 5 and vie < 11:
        bpgM = 1
        bplM = 2
        bpsM = 3
    if vie > 10 and vie < 21:
        bpgM = 2
        bplM = 4
        bpsM = 6
    if vie > 20 and vie < 36:
        bpgM = 3
        bplM = 6
        bpsM = 9
    if vie > 35 and vie < 56:
        bpgM = 4
        bplM = 8
        bpsM = 12
    if vie > 55 and vie < 81:
        bpgM = 5
        bplM = 10
        bpsM = 15
    if vie > 80:
        bpgM = 6
        bplM = 12
        bpsM = 18
    if neant > 5 and neant < 11:
        bmgM = 1
        bmlM = 2
        bmsM = 3
    if neant > 10 and neant < 21:
        bmgM = 2
        bmlM = 4
        bmsM = 6
    if neant > 20 and neant < 36:
        bmgM = 3
        bmlM = 6
        bmsM = 9
    if neant > 35 and neant < 56:
        bmgM = 4
        bmlM = 8
        bmsM = 12
    if neant > 55 and neant < 81:
        bmgM = 5
        bmlM = 10
        bmsM = 15
    if neant > 80:
        bmgM = 6
        bmlM = 12
        bmsM = 18
    
    rmodel.updaterunesandbless(perso["id"],vie,mort,cosmo,chaos,creation,neant,opM,bpsM,bplM,bpgM,bmsM,bmlM,bmgM)

        
class recensemble:
    def GET(self):
        raise web.seeother("/pannel2")
    def POST(self):
        gmodel.acquirelock()
        did = ctx.session.determination
        user = ctx.session.user
        typ = ctx.session.type
        #Need to check timestamp
        if not gmodel.checkaccessdetermination(user,"sens",did):
            gmodel.loselock()
            raise web.seeother("/pannel2")
        persoid = [int(x) for x in ctx.session.personnage.split("P") if x!= ""]
        loads = [int(x) for x in ctx.session.lastload.split("P") if x!= ""]
        inp = web.input()
        for i in range(len(persoid)):
            pid = persoid[i]
            timestamp = loads[i]
            if gmodel.persoindetermination(pid,did) and gmodel.getrune(pid) == "renaissance":
                if gmodel.gettimestamp(pid) > timestamp:
                    gmodel.loselock()
                    raise web.seeother("/pannel2?timeout=1")
                perso = rmodel.getpersosens(pid)
                readandupdatewounds(inp,perso)
                gmodel.updatetimestamp(pid)
        gmodel.loselock()
        raise web.seeother("/pannel2")

class recsens:
    def GET(self):
        raise web.seeother("/pannel2")
    def POST(self):
        gmodel.acquirelock()
        did = ctx.session.determination
        user = ctx.session.user
        typ = ctx.session.type
        if not gmodel.checkaccessdetermination(user,"sens",did):
            gmodel.loselock()
            raise web.seeother("/pannel2")
        pid = ctx.session.personnage
        timestamp = ctx.session.lastload
        if gmodel.gettimestamp(pid) > timestamp:
            gmodel.loselock()
            raise web.seeother("/pannel2?timeout=1")
        #Need to check timestamp
        if gmodel.persoindetermination(pid,did) and gmodel.getrune(pid) == "renaissance":
            inp = web.input()
            perso = rmodel.getpersosens(pid)
            #Update points (runes+liens)
            readandupdatepoints(inp,perso)
            #Update immersion
            reandandupdateimmersion(inp,perso)
            #Update runes offsets
            readandupdateoffsets(inp,perso)
            #Update resplioide (blessures max et dégâts)
            reandandupdateresplioidebless(inp,perso)
            #Update faits
            reandandupdatefaitsrunes(inp,perso)
            #Update runes (inclus ombre pouvoirs)
            updaterunes(perso)
            #Update blessures (inclus temprunes)
            readandupdatewounds(inp,perso)
        gmodel.updatetimestamp(pid)
        gmodel.loselock()
        raise web.seeother("/pannel2")


class export:
    def GET(self):
        did = ctx.session.determination
        user = ctx.session.user
        if not gmodel.checkaccessdetermination(user,"sens",did):
            raise web.seeother("/pannel2")
        inp = web.input(perso=None)
        pid = inp["perso"]
        if pid:
            pid = pid.split("P")[0]
            if pid.isdigit():
                return rview.export(int(pid))
        raise web.seeother("/pannel2")

    
def readandupdateop(inp,perso):
    pid = perso["id"]
    op = perso["op"]
    if hasattr(inp,"op"):
        op = inp["op"]
    perso["op"] = op
    rmodel.updateop(pid,op)
    

class reccellulis:
    def GET(self):
        raise web.seeother("/pannel2")
    def POST(self):
        gmodel.acquirelock()
        did = ctx.session.determination
        user = ctx.session.user
        typ = ctx.session.type
        pid = int(ctx.session.personnage)
        timestamp = ctx.session.lastload
        if gmodel.gettimestamp(pid) > timestamp:
            gmodel.loselock()
            raise web.seeother("/pannel2?timeout=1")
        #Need to check timestamp
        if not gmodel.checkaccesspersonnage(user,pid,did):
            gmodel.loselock()
            raise web.seeother("/pannel2")
        inp = web.input()
        perso = rmodel.getpersosens(pid)
        readandupdatewounds(inp,perso)
        readandupdateop(inp,perso)
        gmodel.updatetimestamp(pid)
        gmodel.loselock()
        raise web.seeother("/pannel2")

def readandupdatename(inp,perso):
    pid = perso["id"]
    nom = perso["nom"]
    if hasattr(inp,"rename"):
        nom = inp["rename"]
        perso["nom"] = nom
        rmodel.updatenom(pid,nom)

def readandupdateditfait(inp,perso):
    fid = -1
    pid = perso["id"]
    if hasattr(inp,"faitid") and inp["faitid"].isdigit(): 
        fid = int(inp["faitid"])
        if rmodel.isofperso("faits",fid,pid):
            if hasattr(inp,"faitdelete") and inp["faitdelete"]:
                rmodel.deletefait(fid)
            else:
                ftext = ""
                fpublic = -1
                if hasattr(inp,"faittxt"):
                    ftext = inp["faittxt"]
                if hasattr(inp,"faitpublic"):
                    fpublic = 1
                else:
                    fpublic = 0
                rmodel.updatefait(fid,ftext,fpublic)

def readandupdateaddfait(inp,perso):
    pid = perso["id"]
    if hasattr(inp,"addfaittxt1") and inp["addfaittxt1"] != "":
        ftext = inp["addfaittxt1"]
        if hasattr(inp,"addfaitpublic1"):
            fpublic = 1
        else:
            fpublic = 0
        rmodel.addfait(pid,ftext,fpublic)
    if hasattr(inp,"addfaittxt2") and inp["addfaittxt2"] != "":
        ftext = inp["addfaittxt2"]
        if hasattr(inp,"addfaitpublic2"):
            fpublic = 1
        else:
            fpublic = 0
        rmodel.addfait(pid,ftext,fpublic)
    if hasattr(inp,"addfaittxt3") and inp["addfaittxt3"] != "":
        ftext = inp["addfaittxt3"]
        if hasattr(inp,"addfaitpublic3"):
            fpublic = 1
        else:
            fpublic = 0
        rmodel.addfait(pid,ftext,fpublic)
    
def readandupdateditlien(inp,perso):
    lid = -1
    pid = perso["id"]
    if hasattr(inp,"lienid") and inp["lienid"].isdigit(): 
        lid = int(inp["lienid"])
        if rmodel.isofperso("liens",lid,pid):
            if hasattr(inp,"liendelete") and inp["liendelete"]:
                rmodel.deletelien(lid)
            else:
                lcible = ""
                lpublic = -1
                lvaleur = -1
                if hasattr(inp,"liencible"):
                    lcible = inp["liencible"]
                if hasattr(inp,"lienpublic"):
                    lpublic = 1
                else:
                    lpublic = 0
                if hasattr(inp,"lienvaleur"):
                    lvaleur= int(inp["lienvaleur"])
                rmodel.updatelien(lid,lcible,lpublic,lvaleur)

def readandupdateaddlien(inp,perso):
    pid = perso["id"]
    if hasattr(inp,"addliencible1") and inp["addliencible1"] != "":
        lcible = inp["addliencible1"]
        if hasattr(inp,"addlienpublic1"):
            lpublic = 1
        else:
            lpublic = 0
        if inp["addlienvaleur1"].isdigit():
            lvaleur = int(inp["addlienvaleur1"])
        else:
            lvaleur = 0 
        rmodel.addlien(pid,lcible,lpublic,lvaleur)
    if hasattr(inp,"addliencible2") and inp["addliencible2"] != "":
        lcible = inp["addliencible2"]
        if hasattr(inp,"addlienpublic2"):
            lpublic = 1
        else:
            lpublic = 0
        if inp["addlienvaleur2"].isdigit():
            lvaleur = int(inp["addlienvaleur2"])
        else:
            lvaleur = 0 
        rmodel.addlien(pid,lcible,lpublic,lvaleur)
            
def readandupdateditresplioide(inp,perso):
    rid = -1
    pid = perso["id"]
    if hasattr(inp,"respid") and inp["respid"].isdigit(): 
        rid = int(inp["respid"])
        if rmodel.isofperso("resplioide",rid,pid):
            if hasattr(inp,"respdelete") and inp["respdelete"]:
                rmodel.deleteresplioide(rid)
            else:
                rtxt = ""
                if hasattr(inp,"resptxt"):
                    rtxt = inp["resptxt"]
                    rmodel.updateresplioide(rid,rtxt)
    
def readandupdateaddresplioide(inp,perso):
    pid = perso["id"]
    if hasattr(inp,"addresptxt1") and inp["addresptxt1"] != "":
        rtxt = inp["addresptxt1"]
        rmodel.addresplioide(pid,rtxt)
    if hasattr(inp,"addresptxt2") and inp["addresptxt2"] != "":
        rtxt = inp["addresptxt2"]
        rmodel.addresplioide(pid,rtxt)



def readandupdateditobjectif(inp,perso):
    oid = -1
    pid = perso["id"]
    if hasattr(inp,"objid") and inp["objid"].isdigit(): 
        oid = int(inp["objid"])
        if rmodel.isofperso("objectifs",oid,pid):
            if hasattr(inp,"objdelete") and inp["objdelete"]:
                rmodel.deleteobjectif(oid)
            else:
                otxt = ""
                if hasattr(inp,"objtxt"):
                    otxt = inp["objtxt"]
                    rmodel.updateobjectif(oid,otxt)
    
def readandupdateaddobjectif(inp,perso):
    pid = perso["id"]
    if hasattr(inp,"addobjtxt1") and inp["addobjtxt1"] != "":
        otxt = inp["addobjtxt1"]
        rmodel.addobjectif(pid,otxt)
    if hasattr(inp,"addobjtxt2") and inp["addobjtxt2"] != "":
        otxt = inp["addobjtxt2"]
        rmodel.addobjectif(pid,otxt)

        
def readandupdateditextra(inp,perso):
    pid = perso["id"]
    if hasattr(inp,"extraid") and inp["extraid"].isdigit(): 
        eid = int(inp["extraid"])
        if rmodel.isofperso("faits_extra",eid,pid):
            if hasattr(inp,"extradelete") and inp["extradelete"]:
                rmodel.deleteextra(eid)
            else:
                etxt = ""
                epublic = -1
                if hasattr(inp,"extratxt"):
                    etxt = inp["extratxt"]
                if hasattr(inp,"extrapublic"):
                    epublic = 1
                else:
                    epublic = 0
                rmodel.updateextra(eid,etxt,epublic)

def readandupdateaddextra(inp,perso):
    pid = perso["id"]
    if hasattr(inp,"addextratxt1") and inp["addextratxt1"] != "":
        etext = inp["addextratxt1"]
        if hasattr(inp,"addextrapublic1"):
            epublic = 1
        else:
            epublic = 0
        rmodel.addextra(pid,etext,epublic)
    if hasattr(inp,"addextratxt2") and inp["addextratxt2"] != "":
        etext = inp["addextratxt2"]
        if hasattr(inp,"addextrapublic2"):
            epublic = 1
        else:
            epublic = 0
        rmodel.addextra(pid,etext,epublic)


def readandupdateditcellulis(inp,perso):
    cid = -1
    pid = perso["id"]
    if hasattr(inp,"celid") and inp["celid"].isdigit(): 
        cid = int(inp["celid"])
        if rmodel.isofperso("faits_cellulis",cid,pid):
            if hasattr(inp,"celdelete") and inp["celdelete"]:
                rmodel.deletefcellulis(cid)
            else:
                ctxt = ""
                if hasattr(inp,"celtxt"):
                    ctxt = inp["celtxt"]
                    rmodel.updatefcellulis(cid,ctxt)
    
def readandupdateaddcellulis(inp,perso):
    pid = perso["id"]
    if hasattr(inp,"addceltxt1") and inp["addceltxt1"] != "":
        ctxt = inp["addceltxt1"]
        rmodel.addfcellulis(pid,ctxt)
    if hasattr(inp,"addceltxt2") and inp["addceltxt2"] != "":
        ctxt = inp["addceltxt2"]
        rmodel.addfcellulis(pid,ctxt)

        
        

        
class modalreccellulis:
    def GET(self):
        raise web.seeother("/pannel2")
    def POST(self):
        gmodel.acquirelock()
        did = ctx.session.determination
        user = ctx.session.user
        typ = ctx.session.type
        pid = int(ctx.session.personnage)
        timestamp = ctx.session.lastload
        if gmodel.gettimestamp(pid) > timestamp:
            gmodel.loselock()
            raise web.seeother("/pannel2?timeout=1")
        #Need to check timestamp
        if not gmodel.checkaccesspersonnage(user,pid,did):
            gmodel.loselock()
            raise web.seeother("/pannel2")
        inp = web.input()
        print(inp)
        perso = rmodel.getpersosens(pid)
        #Rename modal
        readandupdatename(inp,perso)
        #Update faits
        readandupdateditfait(inp,perso)
        readandupdateaddfait(inp,perso)
        #Liens
        readandupdateditlien(inp,perso)
        readandupdateaddlien(inp,perso)
        #Resplioide
        readandupdateditresplioide(inp,perso)
        readandupdateaddresplioide(inp,perso)
        #Objectifs
        readandupdateditobjectif(inp,perso)
        readandupdateaddobjectif(inp,perso)
        #Extra
        readandupdateditextra(inp,perso)
        readandupdateaddextra(inp,perso)
        #Cellulis
        readandupdateditcellulis(inp,perso)
        readandupdateaddcellulis(inp,perso)
        gmodel.updatetimestamp(pid)
        gmodel.loselock()
        raise web.seeother("/pannel2")
