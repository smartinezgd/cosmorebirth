# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship

import web
from web import ctx
import re
import sensconfig
import account.amodel as amodel
import account.aview as aview
import email
import smtplib
from account.aview import render



class index:
    form = web.form.Form(
        web.form.Textbox('login', description="Identifiant :"),
        web.form.Password('password', description="Clé de détermination :"),
    )
    def GET(self):
        form = self.form()
        failedLog = web.input(ok="1").ok
        if failedLog == "1":
            return render.base(aview.login(form,failed=False))
        return render.base(aview.login(form,failed=True))
    def POST(self):
        form = self.form()
        form.validates()
        #Check_password
        login = form.inputs[0].value
        passwd = form.inputs[1].value
        idy = amodel.authentify(login,passwd)
        if idy:
            ctx.session.user=idy
            raise web.seeother('/pannel')
        else:
            raise web.seeother('/?ok=0')

class logout:
    def GET(self):
        ctx.session.kill()
        raise web.seeother('/')

class forget:
    form = web.form.Form(
        web.form.Textbox('login', description="Identifiant :"),
        web.form.Textbox('mail',description="E-mail :")
    )
    def GET(self):
        form = self.form()
        error = web.input(error="0").error
        return render.base(aview.forget(form,error))
    def POST(self):
        form = self.form()
        form.validates()
        login = form.inputs[0].value
        mail = form.inputs[1].value
        error = amodel.checkloginmail(login,mail)
        #check email adress
        if not re.match(r"[^@]+@[^@]+\.[^@]+", mail):
            print(mail)
            error = "1"
        if error == "0":
            if sensconfig.enablemail:
                #New password
                passwd = amodel.newpassword(login)
                message = email.mime.text.MIMEText(sensconfig.messageTemplate.replace("CLE",passwd),"plain","UTF-8")
                message['Subject'] = 'Réinitialisation de clé Como & Rebirth'
                message['From'] = "Cosmo & Rebirth <"+str(sensconfig.fromField)+">"
                message['To'] = mail
                
                s = smtplib.SMTP(sensconfig.smtpServer)
                s.sendmail(sensconfig.fromField, [mail], message.as_string())
                s.quit()
                #send mail for password recovery
                raise web.seeother("/forget?error=3")
            else:
                raise web.seeother("/forget?error=4")
        else:
            raise web.seeother("/forget?error="+error)
            

class neant:
    form = web.form.Form(
        web.form.Textbox('login', description="Identifiant :"),
        web.form.Textbox('nom', description="Nom :"),
        web.form.Password('password', description="Clé de détermination :"),
        web.form.Password('password2', description="Confirmation clé de détermination :"),
        web.form.Textbox('mail', description="Adresse e-mail :"),
    )
    def GET(self):
        form = self.form()
        failedLog = web.input(ok="1").ok
        if failedLog == "1":
            return render.base(aview.neant(form,failed=False))
        return render.base(aview.neant(form,failed=True))
    def POST(self):
        form = self.form()
        form.validates()
        login = form.inputs[0].value
        nom = form.inputs[1].value
        passwd1 = form.inputs[2].value
        passwd2 = form.inputs[3].value
        mail = form.inputs[4].value
        if passwd1 != passwd2:
            raise web.seeother('/neant')
        idy = amodel.checkadd(login,passwd1,nom,mail)
        if idy:
            ctx.session.user=idy
            raise web.seeother('/pannel')
        raise web.seeother('/neant?ok=0')


class account:
    form = web.form.Form(
        web.form.Textbox('nom', description="Nom :"),
        web.form.Password('password', description="Clé de détermination :"),
        web.form.Password('newpassword', description="Nouvelle clé de détermination :"),
        web.form.Textbox('mail', description="Adresse e-mail :"),
    )
    def GET(self):
        user = ctx.session.user
        if not user:
            raise web.seeother('/')
        form = self.form()
        failedLog = web.input(ok="1").ok
        if failedLog == "1":
            return render.base(aview.account(form,user,failed=False))
        return render.base(aview.account(form,user,failed=True))
    def POST(self):
        user = ctx.session.user
        if not user:
            raise web.seeother('/')
        form = self.form()
        form.validates()
        newnom = form.inputs[0].value
        passwd = form.inputs[1].value
        newpasswd = form.inputs[2].value
        mail = form.inputs[3].value
        idy = amodel.authentify(amodel.loginfromid(user),passwd)
        if not idy:
            raise web.seeother("/account?ok=0")
        inp = web.input(suppr1=False,suppr2=False,suppr3=False)
        if inp.suppr1 and inp.suppr2 and inp.suppr3:
            amodel.anihilate(user)
            raise web.seeother("/")
        amodel.updateaccount(user,newnom,newpasswd,mail)
        raise web.seeother('/pannel')
