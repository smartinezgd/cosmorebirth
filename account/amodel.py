# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship



import web
import time
import hashlib
import random
import string
from model import determinationsof,unbindcellulis

db = web.database(dbn="sqlite",db="cosmo.sqlite")


#Authentication
def authentify(login,password):
    users = db.select("cellulis",where="login='"+login+"'")
    passwd = hashlib.sha1()
    passwd.update(password)
    passwd = passwd.hexdigest()
    for u in users:
        if u.password == passwd:
            return u.id
    return None

#Check if cellulis can be added 
def checkadd(logn,password,nom1,mail):
    users = db.select("cellulis",where="login='"+logn+"'")
    for u in users:
        return None
    passwd = hashlib.sha1()
    passwd.update(password)
    passwd = passwd.hexdigest()
    db.insert("cellulis",login=logn,password=passwd,nom=nom1,email=mail)
    users = db.select("cellulis",where="login='"+logn+"'")
    for u in users:
        return u.id

#Update cellulis info
def updateaccount(uid,newnom,newpasswd,mail):
    if newnom and newnom != "":
        db.update("cellulis",where="id='"+str(uid)+"'",nom=newnom)
    if mail and mail != "":
        db.update("cellulis",where="id='"+str(uid)+"'",email=mail)
    if newpasswd and newpasswd != "":
        passwd = hashlib.sha1()
        passwd.update(newpasswd)
        passwd = passwd.hexdigest()
        db.update("cellulis",where="id='"+str(uid)+"'",password=passwd)

#Cast cellulis away
def anihilate(uid):
    dets = determinationsof(uid)
    #end determinations where user is sens
    for d in dets[0]:
        enddetermination(d[1])
    #unbind user on otherdeterminations
    for d in dets[1]:
        unbindcellulis(uid,d[1])
    db.delete("cellulis",where="id='"+str(uid)+"'")


def checkloginmail(login,mail):
    users = db.select("cellulis",where="login='"+str(login)+"'")
    for u in users:
        if u.email == mail:
            return "0"
        else:
            return "1"
    return "2"

def newpassword(login):
    newpasswd = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    passwd = hashlib.sha1()
    passwd.update(newpasswd)
    passwd = passwd.hexdigest()
    db.update("cellulis",where="login='"+str(login)+"'",password=passwd)
    return newpasswd


def getuserinfo(uid):
    users = db.select("cellulis",where="id='"+str(uid)+"'")
    for u in users:
        return u
    return None

        
def loginfromid(uid):
    users = db.select("cellulis",where="id='"+str(uid)+"'")
    for u in users:
        return u.login
    return None
