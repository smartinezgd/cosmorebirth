# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship


import web
import account.amodel as amodel
import model
import renaissance
import renaissance.rview

render = web.template.render("templates/general/")

#Pannel

def pannel(user):
    nom = amodel.getuserinfo(user).nom 
    determinations=model.determinationsof(user)
    return render.pannel(nom,determinations)

#Creation

def creation(form,error):
    return render.creation(form,error)

#Handling cellulis as Sens

def cellulis(did):
    cle = model.clefromid(did)
    cellulis = model.cellulistree(did)
    return render.sens.cellulis(cle,cellulis)

#Pannel2

def pannel2(user,typ,did,pid,access,rune,timeout=False):
    nom = model.nomfromid(user)
    cle = model.clefromid(did)
    if typ == "cellulis":
        cellulistree = model.cellulistree(did)
        perso = None
        if rune == "renaissance":
            if access == "private":
                perso = renaissance.rview.privateperso(pid,timeout)
            if access == "public":
                perso = renaissance.rview.publicperso(pid)
        if rune == "neant":
            pass
        if rune == "chaos":
            pass
        return render.cellulis.pannel(nom, cle, cellulistree,perso)
    if typ == "sens":
        persoid=[]
        if pid:
            lpid = pid.split("P")
            for p in lpid:
                if p != "":
                    persoid.append(int(p))
        subpage = None
        if rune == "renaissance":
            if len(persoid) > 1:
                subpage = renaissance.rview.ensemble(persoid,timeout)
            elif len(persoid) == 1:
                subpage = renaissance.rview.sensperso(persoid[0],timeout)
        if rune == "neant":
            pass
        if rune == "chaos":
            pass
        cellulis = model.cellulistreeofrune(did,rune)
        return render.sens.pannel(cle,cellulis,persoid,rune,subpage)





