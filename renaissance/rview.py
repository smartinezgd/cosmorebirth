# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship



import web
import renaissance.rmodel as rmodel

render = web.template.render("templates/renaissance/")

#Handling personnages for cellulis

def privateperso(pid,timeout):
    perso = rmodel.getperso(pid)
    return render.cellulis.viewperso(perso,timeout) 

def publicperso(pid):
    perso = rmodel.getpersopublic(pid)
    return render.cellulis.viewpublicperso(perso) 


#Handling personnages for sens

def ensemble(persoid,timeout):
    persos = []
    for pid in persoid:
        persos.append(rmodel.getpersosens(pid))
    return render.sens.vueensemble(persos,timeout)


def sensperso(pid,timeout):
    perso = rmodel.getpersosens(pid)
    return render.sens.viewperso(perso,timeout)

def export(pid):
    perso = rmodel.getpersosens(pid)
    return render.sens.export(perso)

    
def celluliseditbugr(pid):
    p = model.getperso(pid)
    perso = model.getbugclasse1(pid,p.nom)
    return render.celluliseditbugr(perso)





