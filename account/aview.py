# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship


import web
import account.amodel as amodel

render = web.template.render("templates/general/")

#Authentatication

def login(form,failed):
    return render.account.login(form,failed=failed)

def neant(form,failed):
    return render.account.neant(form,failed=failed)

def forget(form,error):
    return render.account.forget(form,error)

def account(form,user,failed):
    user  = amodel.getuserinfo(user)
    return render.account.account(form,user,failed)
