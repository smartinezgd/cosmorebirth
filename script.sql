/* This file is part of Comso & Rebirth, a web application for Sens
 Hexalogie, role-playing game by Romaric Birand.

 Cosmo & Rebirth is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details (COPYING.txt).
 
 See CREDITS.txt for credits of authorship */


/* Run sqlite3 cosmo.sqlite

   .read script.sql */


/* Tables fonctions generiques (comptes, tables, etc ...) */

--                        id du cellulis        nom        login        hash mdp        email
 create table cellulis (id integer primary key, nom text, login text, password text, email text);

--                           id de la determination    id de sens  "nom" de la determination
create table determinations (id integer primary key, sens integer, cle text);

--                           id du cellulis     id de la determination
create table detercellulis (cellulis integer, determination integer);

--                           id du personnage   date derniere modif  nom      id du cellulis   id de la determination   type  
create table personnages (id integer primary key, updtime integer, nom text, cellulis integer, determination integer, rune text);


/* Tables personnages Renaissance / Mort */

create table renaissance (personnage integer, vie integer, mort integer, cosmo integer, chaos integer, creation integer, neant integer, vieoff integer, mortoff integer, cosmooff integer, chaosoff integer, creationoff integer, neantoff integer, tvie integer, tmort integer, tcosmo integer, tchaos integer, tcreation integer, tneant integer, immersionp integer, immersionn integer, bps integer, bpl integer, bpg integer, bms integer, bml integer, bmg integer, bpsm integer, bplm integer, bpgm integer, bmsm integer, bmlm integer, bmgm integer, resplioide integer, resplioidem integer, resplioided integer, pointsr integer, pointsl integer, ombrepouvoir integer, ombrepouvoirm integer);

create table resplioide (id integer primary key autoincrement, personnage integer, texte text);

create table faits (id integer primary key autoincrement, personnage integer, texte text, vie integer, mort integer, cosmo integer, chaos integer, creation integer, neant integer, publique integer);

create table faits_cellulis (id integer primary key autoincrement, personnage integer, texte text);

create table faits_extra (id integer primary key autoincrement, personnage integer, texte text, publique integer);

create table liens (id integer primary key autoincrement, personnage integer, cible text, valeur integer, publique integer);

create table objectifs (id integer primary key autoincrement, personnage integer, texte text);

/* Tables personnages N�ant */

-- create table neant (personnage integer, eau integer, feu integer, lumiere integer, ombre integer, terre integer, air integer, relation integer, acide integer, rayonnement integer, rayonnementm integer);

-- create table liens (id integer primary key, personnage integer, valeur integer, cible text, element integer);

-- create table apprivoises (personnage integer, myriadien integer);

-- create table myriadien (id integer primary key, nom text, eau integer, feu integer, lumiere integer, ombre integer, terre integer, air integer, relation integer, acide integer);


-- /* Tables personnages Cosmo / Chaos */

-- create table cosmo (personnage integer, vie integer, mort integer, cosmo integer, chaos integer, creation integer, neant integer, vieoff integer, mortoff integer, cosmooff integer, chaosoff integer, creationoff integer, neantoff integer, immersionp integer, immersionn integer, bps integer, bpl integer, bpg integer, bms integer, bml integer, bmg integer, bpsm integer, bplm integer, bpgm integer, bmsm integer, bmlm integer, bmgm integer, resplioide integer, resplioidem integer, pointsr integer, pointsl integer, ombrepouvoir integer, ombrepouvoirm integer);

