# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship

import web
import time
import hashlib
import random
import string

db = web.database(dbn="sqlite",db="cosmo.sqlite")

def addfait(pid,text,public=0):
    db.insert("faits",personnage=pid,texte=text,vie=0,mort=0,cosmo=0,chaos=0,creation=0,neant=0,publique=public)

def newbugrebirth(cid,did):
    utime = int(time.time())
    pid = db.insert("personnages", nom="Inconnu", cellulis=cid, determination=did, updtime=utime,rune="renaissance")
    db.insert("renaissance",personnage=pid,vie=1,mort=1,cosmo=1,chaos=1,creation=1,neant=1,vieoff=0,mortoff=0,cosmooff=0,chaosoff=0,creationoff=0,neantoff=0,tvie=1,tmort=1,tcosmo=1,tchaos=1,tcreation=1,tneant=1,immersionp=0,immersionn=0,bps=0,bpl=0,bpg=0,bms=0,bml=0,bmg=0,bpsm=1,bplm=1,bpgm=1,bmsm=1,bmlm=1,bmgm=1,pointsr=150,pointsl=0,resplioide=0,resplioidem=3,resplioided=1,ombrepouvoir=0,ombrepouvoirm=0)
    addfait(pid,"Je suis un Bug",1)
    addfait(pid,"Je suis ne le 14 Decembre 996",1)
    addfait(pid,"Je n'ai pas d'ombre",1)
    addfait(pid,"Je suis membre de la Resistance",1)
    addfait(pid,"Je vis au pole Sud, a la base militaire",1)
    addfait(pid,"Je suis membre de la division : bugs",1)
    addfait(pid,"Mes parents sont decedes dans la nuit du 3 Janvier 1001",1)

def newbugdeath(cid,did):
    utime = int(time.time())
    pid = db.insert("personnages", nom="Inconnu", cellulis=cid, determination=did, updtime=utime,rune="renaissance")
    db.insert("renaissance",personnage=pid,vie=1,mort=1,cosmo=1,chaos=1,creation=1,neant=1,vieoff=0,mortoff=0,cosmooff=0,chaosoff=0,creationoff=0,neantoff=0,tvie=1,tmort=1,tcosmo=1,tchaos=1,tcreation=1,tneant=1,immersionp=0,immersionn=0,bps=0,bpl=0,bpg=0,bms=0,bml=0,bmg=0,bpsm=1,bplm=1,bpgm=1,bmsm=1,bmlm=1,bmgm=1,pointsr=150,pointsl=0,resplioide=0,resplioidem=3,resplioided=1,ombrepouvoir=0,ombrepouvoirm=0)
    addfait(pid,"Je suis un Bug",1)
    addfait(pid,"Je suis ne le 26 Fevrier 1002",1)
    addfait(pid,"Je n'ai pas d'ombre",1)
    addfait(pid,"Je suis un(e) orphelin(e) de Doxisteron",1)

def newfragile(cid,did):
    utime = int(time.time())
    pid = db.insert("personnages", nom="Inconnu", cellulis=cid, determination=did, updtime=utime,rune="renaissance")
    db.insert("renaissance",personnage=pid,vie=1,mort=1,cosmo=1,chaos=1,creation=1,neant=1,vieoff=0,mortoff=0,cosmooff=0,chaosoff=0,creationoff=0,neantoff=0,tvie=1,tmort=1,tcosmo=1,tchaos=1,tcreation=1,tneant=1,immersionp=0,immersionn=0,bps=0,bpl=0,bpg=0,bms=0,bml=0,bmg=0,bpsm=1,bplm=1,bpgm=1,bmsm=1,bmlm=1,bmgm=1,pointsr=150,pointsl=0,resplioided=1,resplioide=0,resplioidem=3,ombrepouvoir=0,ombrepouvoirm=0)
    addfait(pid,"Nous sommes un Fragile",1)
    addfait(pid,"Nous possedons le pouvoir supreme de la Vue",1)
    addfait(pid,"Myphos Quadria est notre boureau",1)
    addfait(pid,"Nous ne connaissons rien de nos origines",1)
    addfait(pid,"Nous recherchons nos origines",1)
    addfait(pid,"Nous pouvons lire les sons dans les sillons de l'air",1)
    addfait(pid,"Nous percevons l'unite de l'energie",1)
    addfait(pid,"Nos yeux sont notre seule faiblesse",1)

def newquadrilla(cid,did):
    utime = int(time.time())
    pid = db.insert("personnages", nom="Inconnu", cellulis=cid, determination=did, updtime=utime,rune="renaissance")
    db.insert("renaissance",personnage=pid,vie=1,mort=1,cosmo=1,chaos=1,creation=1,neant=1,vieoff=0,mortoff=0,cosmooff=0,chaosoff=0,creationoff=0,neantoff=0,tvie=1,tmort=1,tcosmo=1,tchaos=1,tcreation=1,tneant=1,immersionp=0,immersionn=0,bps=0,bpl=0,bpg=0,bms=0,bml=0,bmg=0,bpsm=1,bplm=1,bpgm=1,bmsm=1,bmlm=1,bmgm=1,pointsr=150,pointsl=0,resplioide=0,resplioidem=0,resplioided=0,ombrepouvoir=0,ombrepouvoirm=0)
    addfait(pid,"Je suis un Quadrilla",1)
    addfait(pid,"Je proviens de l'ombre-monde",1)
    addfait(pid,"La protection de ma Rune est le sens de mon existence",1)


def getperso(pid):
    perso = {}
    persos = db.select("personnages",where="id='"+str(pid)+"'")
    for p in persos:
        perso["nom"] = p.nom
        perso["id"] = pid
    persos = db.select("renaissance",where="personnage='"+str(pid)+"'")
    for p in persos:
        perso["vievalue"] = p.tvie
        perso["mortvalue"] = p.tmort
        perso["cosmovalue"] = p.tcosmo
        perso["chaosvalue"] = p.tchaos
        perso["creationvalue"] = p.tcreation
        perso["neantvalue"] = p.tneant
        postring = " - "+str(10*(p.bpg+p.bmg))+" (bg) + "+str(p.immersionp - p.immersionn)+" (imm)"
        perso["viestring"] = str(p.vie)+postring
        perso["mortstring"] = str(p.mort)+postring
        perso["cosmostring"] = str(p.cosmo)+postring
        perso["chaosstring"] = str(p.chaos)+postring
        perso["creationstring"] = str(p.creation)+postring
        perso["neantstring"] = str(p.neant)+postring
        perso["op"] = p.ombrepouvoir
        perso["opM"] = p.ombrepouvoirm
        perso["immersionP"] = p.immersionp
        perso["immersionN"] = p.immersionn
        perso["bps"] = p.bps
        perso["bpl"] = p.bpl
        perso["bpg"] = p.bpg
        perso["bms"] = p.bms
        perso["bml"] = p.bml
        perso["bmg"] = p.bmg
        perso["bpsM"] = p.bpsm
        perso["bplM"] = p.bplm
        perso["bpgM"] = p.bpgm
        perso["bmsM"] = p.bmsm
        perso["bmlM"] = p.bmlm
        perso["bmgM"] = p.bmgm
        perso["resplioideb"] = p.resplioide
        perso["resplioideM"] = p.resplioidem
        perso["resplioideD"] = p.resplioided
        perso["liensp"] = p.pointsl
        break
    faits = db.select("faits",where="personnage='"+str(pid)+"'")
    perso["faits"] = []
    n = 0
    for f in faits:
        f0 = {}
        n+=1
        f0["n"] = n
        f0["texte"] = f.texte
        if f.publique == 1:
            f0["prive"] = '<img style="vertical-align: middle;" src="static/public.png" height="32" width="32" alt="publique">'
        else:
            f0["prive"] = '<img style="vertical-align: middle;" src="static/private.png" height="32" width="32" alt="privé">'
        f0["id"] = f.id
        perso["faits"].append(f0)
    liens = db.select("liens",where="personnage='"+str(pid)+"'")
    perso["liens"] = []
    totval = 0
    n = 0
    for l in liens:
        l0 = {}
        n+=1
        l0["n"] = n
        l0["cible"] = l.cible
        l0["valeur"] = l.valeur
        totval+=int(l.valeur)
        if l.publique == 1:
            l0["prive"] = '<img style="vertical-align: middle;" src="static/public.png" height="32" width="32" alt="publique">'
        else:
            l0["prive"] = '<img style="vertical-align: middle;" src="static/private.png" height="32" width="32" alt="privé">'
        l0["id"] = l.id
        perso["liens"].append(l0)
    perso["lienspr"] = perso["liensp"] - totval
    fcellulis = db.select("faits_cellulis",where="personnage='"+str(pid)+"'")
    perso["cfaits"] = []
    n = 0
    for c in fcellulis:
        c0 = {}
        n+=1
        c0["n"] = n
        c0["texte"] = c.texte
        c0["id"] = c.id
        perso["cfaits"].append(c0)
    fextra = db.select("faits_extra",where="personnage='"+str(pid)+"'")
    perso["efaits"] = []
    n=0
    for e in fextra:
        e0 = {}
        n+=1
        e0["n"] = n
        e0["texte"] = e.texte
        if e.publique == 1:
            e0["prive"] = '<img style="vertical-align: middle;" src="static/public.png" height="32" width="32" alt="publique">'
        else:
            e0["prive"] = '<img style="vertical-align: middle;" src="static/private.png" height="32" width="32" alt="privé">'
        e0["id"] = e.id
        perso["efaits"].append(e0)
    fresp = db.select("resplioide",where="personnage='"+str(pid)+"'")
    perso["resplioide"] = []
    n = 0
    for r in fresp:
        r0 = {}
        n+=1
        r0["n"] = n
        r0["texte"] = r.texte
        r0["id"] = r.id
        perso["resplioide"].append(r0)
    objectifs = db.select("objectifs",where="personnage='"+str(pid)+"'")
    perso["objectifs"] = []
    n = 0
    for o in objectifs:
        o0 = {}
        n+=1
        o0["n"] = n
        o0["texte"] = o.texte
        o0["id"] = o.id
        perso["objectifs"].append(o0)
    return perso



def getpersopublic(pid):
    perso = {}
    persos = db.select("personnages",where="id='"+str(pid)+"'")
    for p in persos:
        perso["nom"] = p.nom
        perso["id"] = pid
    persos = db.select("renaissance",where="personnage='"+str(pid)+"'")
    for p in persos:
        perso["vievalue"] = p.tvie
        perso["mortvalue"] = p.tmort
        perso["cosmovalue"] = p.tcosmo
        perso["chaosvalue"] = p.tchaos
        perso["creationvalue"] = p.tcreation
        perso["neantvalue"] = p.tneant
        postring = " - "+str(10*(p.bpg+p.bmg))+" (bg) + "+str(p.immersionp - p.immersionn)+" (imm)"
        perso["viestring"] = str(p.vie)+postring
        perso["mortstring"] = str(p.mort)+postring
        perso["cosmostring"] = str(p.cosmo)+postring
        perso["chaosstring"] = str(p.chaos)+postring
        perso["creationstring"] = str(p.creation)+postring
        perso["neantstring"] = str(p.neant)+postring
        perso["op"] = p.ombrepouvoir
        perso["opM"] = p.ombrepouvoirm
        perso["immersionP"] = p.immersionp
        perso["immersionN"] = p.immersionn
        perso["bps"] = p.bps
        perso["bpl"] = p.bpl
        perso["bpg"] = p.bpg
        perso["bms"] = p.bms
        perso["bml"] = p.bml
        perso["bmg"] = p.bmg
        perso["bpsM"] = p.bpsm
        perso["bplM"] = p.bplm
        perso["bpgM"] = p.bpgm
        perso["bmsM"] = p.bmsm
        perso["bmlM"] = p.bmlm
        perso["bmgM"] = p.bmgm
        perso["resplioideb"] = p.resplioide
        perso["resplioideM"] = p.resplioidem
        perso["resplioideD"] = p.resplioided
        perso["liensp"] = p.pointsl
        break
    faits = db.select("faits",where="personnage='"+str(pid)+"' AND publique=1")
    perso["faits"] = []
    for f in faits:
        f0 = {}
        f0["texte"] = f.texte
        perso["faits"].append(f0)
    liens = db.select("liens",where="personnage='"+str(pid)+"' AND publique=1")
    perso["liens"] = []
    for l in liens:
        l0 = {}
        l0["cible"] = l.cible
        l0["valeur"] = l.valeur
        perso["liens"].append(l0)
    fextra = db.select("faits_extra",where="personnage='"+str(pid)+"' AND publique=1")
    perso["efaits"] = []
    for e in fextra:
        e0 = {}
        e0["texte"] = e.texte
        perso["efaits"].append(e0)
    fresp = db.select("resplioide",where="personnage='"+str(pid)+"'")
    perso["resplioide"] = []
    for r in fresp:
        r0 = {}
        r0["texte"] = r.texte
        perso["resplioide"].append(r0)
    return perso

def deleteperso(pid):
     db.delete("renaissance",where="personnage='"+str(pid)+"'")
     db.delete("faits",where="personnage='"+str(pid)+"'")
     db.delete("faits_cellulis",where="personnage='"+str(pid)+"'")
     db.delete("faits_extra",where="personnage='"+str(pid)+"'")
     db.delete("liens",where="personnage='"+str(pid)+"'")
     db.delete("resplioide",where="personnage='"+str(pid)+"'")
     db.delete("objectifs",where="personnage='"+str(pid)+"'")

def getpersosens(pid):
    perso = {}
    persos = db.select("personnages",where="id='"+str(pid)+"'")
    for p in persos:
        perso["nom"] = p.nom
        perso["id"] = pid
        perso["rune"] = p.rune
    persos = db.select("renaissance",where="personnage='"+str(pid)+"'")
    for p in persos:
        perso["vievalue"] = p.tvie
        perso["mortvalue"] = p.tmort
        perso["cosmovalue"] = p.tcosmo
        perso["chaosvalue"] = p.tchaos
        perso["creationvalue"] = p.tcreation
        perso["neantvalue"] = p.tneant
        perso["viebase"] = p.vie
        perso["mortbase"] = p.mort
        perso["cosmobase"] = p.cosmo
        perso["chaosbase"] = p.chaos
        perso["creationbase"] = p.creation
        perso["neantbase"] = p.neant
        postring = " - "+str(10*(p.bpg+p.bmg))+" (bg) + "+str(p.immersionp - p.immersionn)+" (imm)"
        perso["viestring"] = str(p.vie)+postring
        perso["mortstring"] = str(p.mort)+postring
        perso["cosmostring"] = str(p.cosmo)+postring
        perso["chaosstring"] = str(p.chaos)+postring
        perso["creationstring"] = str(p.creation)+postring
        perso["neantstring"] = str(p.neant)+postring
        perso["vieoffset"] = p.vieoff
        perso["mortoffset"] = p.mortoff
        perso["cosmooffset"] = p.cosmooff
        perso["chaosoffset"] = p.chaosoff
        perso["creationoffset"] = p.creationoff
        perso["neantoffset"] = p.neantoff
        perso["op"] = p.ombrepouvoir
        perso["opM"] = p.ombrepouvoirm
        perso["immersionP"] = p.immersionp
        perso["immersionN"] = p.immersionn
        perso["bps"] = p.bps
        perso["bpl"] = p.bpl
        perso["bpg"] = p.bpg
        perso["bms"] = p.bms
        perso["bml"] = p.bml
        perso["bmg"] = p.bmg
        perso["bpsM"] = p.bpsm
        perso["bplM"] = p.bplm
        perso["bpgM"] = p.bpgm
        perso["bmsM"] = p.bmsm
        perso["bmlM"] = p.bmlm
        perso["bmgM"] = p.bmgm
        perso["resplioideb"] = p.resplioide
        perso["resplioideM"] = p.resplioidem
        perso["resplioideD"] = p.resplioided
        perso["liensp"] = p.pointsl
        perso["points"] = p.pointsr
        break
    faits = db.select("faits",where="personnage='"+str(pid)+"'")
    perso["faits"] = []
    n = 0
    for f in faits:
        f0 = {}
        n+=1
        f0["n"] = n
        f0["texte"] = f.texte
        if f.publique == 1:
            f0["prive"] = '<img style="vertical-align: middle;" src="static/public.png" height="32" width="32" alt="publique">'
        else:
            f0["prive"] = '<img style="vertical-align: middle;" src="static/private.png" height="32" width="32" alt="privé">'
        f0["id"] = f.id
        f0["vie"] = f.vie
        f0["mort"] = f.mort
        f0["cosmo"] = f.cosmo
        f0["chaos"] = f.chaos
        f0["creation"] = f.creation
        f0["neant"] = f.neant
        perso["faits"].append(f0)
    liens = db.select("liens",where="personnage='"+str(pid)+"'")
    perso["liens"] = []
    totval = 0
    n = 0
    for l in liens:
        l0 = {}
        n+=1
        l0["n"] = n
        l0["cible"] = l.cible
        l0["valeur"] = l.valeur
        totval+=int(l.valeur)
        if l.publique == 1:
            l0["prive"] = '<img style="vertical-align: middle;" src="static/public.png" height="32" width="32" alt="publique">'
        else:
            l0["prive"] = '<img style="vertical-align: middle;" src="static/private.png" height="32" width="32" alt="privé">'
        l0["id"] = l.id
        perso["liens"].append(l0)
    perso["lienspr"] = perso["liensp"] - totval
    fcellulis = db.select("faits_cellulis",where="personnage='"+str(pid)+"'")
    perso["cfaits"] = []
    n = 0
    for c in fcellulis:
        c0 = {}
        n+=1
        c0["n"] = n
        c0["texte"] = c.texte
        c0["id"] = c.id
        perso["cfaits"].append(c0)
    fextra = db.select("faits_extra",where="personnage='"+str(pid)+"'")
    perso["efaits"] = []
    n = 0
    for e in fextra:
        e0 = {}
        n+=1
        e0["n"] = n
        e0["texte"] = e.texte
        if e.publique == 1:
            e0["prive"] = '<img style="vertical-align: middle;" src="static/public.png" height="32" width="32" alt="publique">'
        else:
            e0["prive"] = '<img style="vertical-align: middle;" src="static/private.png" height="32" width="32" alt="privé">'
        e0["id"] = e.id
        perso["efaits"].append(e0)
    fresp = db.select("resplioide",where="personnage='"+str(pid)+"'")
    perso["resplioide"] = []
    n = 0
    for r in fresp:
        r0 = {}
        n+=1
        r0["n"] = n
        r0["texte"] = r.texte
        r0["id"] = r.id
        perso["resplioide"].append(r0)
    objectifs = db.select("objectifs",where="personnage='"+str(pid)+"'")
    perso["objectifs"] = []
    n = 0
    for o in objectifs:
        o0 = {}
        n+=1
        o0["n"] = n
        o0["texte"] = o.texte
        o0["id"] = o.id
        perso["objectifs"].append(o0)
    return perso


def updatewounds(pid,Bbps,Bbpl,Bbpg,Bbms,Bbml,Bbmg,Bresp):
    db.update("renaissance",where="personnage='"+str(pid)+"'",bps=Bbps,bpl=Bbpl,bpg=Bbpg,bms=Bbms,bml=Bbml,bmg=Bbmg,resplioide=Bresp)

def updatetemprunes(pid,Ttvie,Ttmort,Ttcosmo,Ttchaos,Ttcreation,Ttneant):
    db.update("renaissance",where="personnage='"+str(pid)+"'",tvie=Ttvie,tmort=Ttmort,tcosmo=Ttcosmo,tchaos=Ttchaos,tcreation=Ttcreation,tneant=Ttneant)

def updatepoints(pid,Ppointsr,Ppointsl):
    db.update("renaissance",where="personnage='"+str(pid)+"'",pointsr=Ppointsr,pointsl=Ppointsl)

def updateimmersion(pid,immP,immN):
    db.update("renaissance",where="personnage='"+str(pid)+"'",immersionp=immP,immersionn=immN)

def updateresplioidebless(pid,bless,arm):
    db.update("renaissance",where="personnage='"+str(pid)+"'",resplioided=bless,resplioidem=arm)

def updatefaitrunes(fid,Rvie,Rmort,Rcosmo,Rchaos,Rcreation,Rneant):
    db.update("faits",where="id='"+str(fid)+"'",vie=Rvie,mort=Rmort,cosmo=Rcosmo,chaos=Rchaos,creation=Rcreation,neant=Rneant)

def updaterunesandbless(pid,Rvie,Rmort,Rcosmo,Rchaos,Rcreation,Rneant,opM,bpsM,bplM,bpgM,bmsM,bmlM,bmgM):
    db.update("renaissance",where="personnage='"+str(pid)+"'",vie=Rvie,mort=Rmort,cosmo=Rcosmo,chaos=Rchaos,creation=Rcreation,neant=Rneant,ombrepouvoirm=opM,bpsm=bpsM,bplm=bplM,bpgm=bpgM,bmsm=bmsM,bmlm=bmlM,bmgm=bmgM)

def updateoffsets(pid,vie,mort,cosmo,chaos,creation,neant):
    db.update("renaissance",where="personnage='"+str(pid)+"'",vieoff=vie,mortoff=mort,cosmooff=cosmo,chaosoff=chaos,creationoff=creation,neantoff=neant)
    
def updateop(pid,op):
    db.update("renaissance",where="personnage='"+str(pid)+"'",ombrepouvoir=op)

def updatenom(pid,Nnom):
    db.update("personnages",where="id='"+str(pid)+"'",nom=Nnom)

def isofperso(table,elemid,pid):
    els = db.select(table,where="id='"+str(elemid)+"' AND personnage='"+str(pid)+"'")
    for e in els:
        return True
    return False

def deletefait(fid):
    db.delete("faits",where="id='"+str(fid)+"'")

def updatefait(fid,ftext,fpublic):
    if ftext != "" and fpublic != -1:
        db.update("faits",where="id='"+str(fid)+"'",texte=ftext,publique=fpublic)
    elif ftext != "":
        db.update("faits",where="id='"+str(fid)+"'",texte=ftext)
    elif fpublic != -1:
        db.update("faits",where="id='"+str(fid)+"'",publique=fpublic)


def deletelien(lid):
    db.delete("liens",where="id='"+str(lid)+"'")

def updatelien(lid,lcible,lpublic,lvaleur):
    if lcible != "":
        db.update("liens",where="id='"+str(lid)+"'",cible=lcible)
    if lvaleur != -1:
        db.update("liens",where="id='"+str(lid)+"'",valeur=lvaleur)
    if lpublic != -1:
        db.update("liens",where="id='"+str(lid)+"'",publique=lpublic)

def addlien(pid,lcible,lpublic,lvaleur):
    db.insert("liens",personnage=pid,cible=lcible,publique=lpublic,valeur=lvaleur)


def deleteresplioide(rid):
    db.delete("resplioide",where="id='"+str(rid)+"'")

def updateresplioide(rid,rtxt):
    if rtxt != "":
        db.update("resplioide",where="id='"+str(rid)+"'",texte=rtxt)

def addresplioide(pid,rtxt):
    db.insert("resplioide",personnage=pid,texte=rtxt)
        

def deleteobjectif(oid):
    db.delete("objectifs",where="id='"+str(oid)+"'")

def updateobjectif(oid,otxt):
    if otxt != "":
        db.update("objectifs",where="id='"+str(oid)+"'",texte=otxt)

def addobjectif(pid,otxt):
    db.insert("objectifs",personnage=pid,texte=otxt)
        

def deleteextra(eid):
    db.delete("faits_extra",where="id='"+str(eid)+"'")

def updateextra(eid,etxt,epublic):
    if etxt != "" and epublic != -1:
        db.update("faits_extra",where="id='"+str(eid)+"'",texte=etxt,publique=epublic)
    elif etxt != "":
        db.update("faits_extra",where="id='"+str(eid)+"'",texte=etxt)
    elif epublic != -1:
        db.update("faits_extra",where="id='"+str(eid)+"'",publique=epublic)

def addextra(pid,etxt,epublic):
    db.insert("faits_extra",personnage=pid,texte=etxt,publique=epublic)    


def deleteofcellulis(cid):
    db.delete("faits_cellulis",where="id='"+str(cid)+"'")

def updatefcellulis(cid,ctxt):
    if ctxt != "":
        db.update("faits_cellulis",where="id='"+str(cid)+"'",texte=ctxt)

def addfcellulis(pid,ctxt):
    db.insert("faits_cellulis",personnage=pid,texte=ctxt)
        
