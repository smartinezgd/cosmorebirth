# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship



import web
import time
import hashlib
import random
import string
import tempfile
import os
import renaissance
import renaissance.rmodel as rmodel

db = web.database(dbn="sqlite",db="cosmo.sqlite")

# Determination management
##########################

#Get all determinations where cellulis idy is implicated
def determinationsof(idy):
    sensd = db.select("determinations",where="sens='"+str(idy)+"'")
    d1 = [(x.cle,x.id,"sens") for x in sensd]
    d2 = []
    cellulisd = db.select("detercellulis",where="cellulis='"+str(idy)+"'")
    for lien in cellulisd:
        ds  = db.select("determinations",where="id='"+str(lien.determination)+"'")
        for x in ds:
            d2.append((x.cle,x.id,"cellulis"))
    return (d1,d2)

#Create determination
def newdetermination(cellulis,sensid):
    ids = []
    for login in cellulis:
        users = db.select("cellulis",where="login='"+login+"'")
        for u in users:
            ids.append(u.id)
    if len(ids) == 0:
        return None
    dcle = newcle()
    db.insert("determinations",sens=sensid,cle=dcle)
    ds = db.select("determinations",where="cle='"+dcle+"'")
    did = None
    for d in ds:
        did = d.id
    for u in ids:
        db.insert("detercellulis",cellulis=u,determination=did)
    return did

#New key for determination
def newcle():
    keys = [x.cle for x in db.select('determinations')]
    m = hashlib.md5()
    m.update(time.strftime("%x:%H:%M:%S"))
    k = m.hexdigest()[0:10]
    if k in keys:
        ok = False
        while not ok:
            for c in ["a","X","j","U","i","6","2","3","4","p","Z"]:
                if k+c not in keys:
                    k+=c
                    ok = True
    return k

#Check if user ass access of type typ to determination of id id
def checkaccessdetermination(user,typ,did):
    deters = db.select("determinations",where="id='"+str(did)+"'")
    if typ == "sens":
        for d in deters:
            if d.sens == user:
                return True
    if typ == "cellulis":
        liens = db.select("detercellulis",where="cellulis='"+str(user)+"' AND determination='"+str(did)+"'")
        for x in liens:
            return True
    return False

#End determination
def enddetermination(did):
    cellulis = db.select("detercellulis",where="determination='"+str(did)+"'")
    cels = []
    for c in cellulis:
        cels.append(c.cellulis)
    for c in cels:
        unbindcellulis(c,did)
    db.delete("determinations",where="id='"+str(did)+"'")

#Remove cellulis cid from determination did
def unbindcellulis(cid,did):
    #Delete personnages of cellulis
    persos = db.select("personnages",where="cellulis='"+str(cid)+"' AND determination='"+str(did)+"'")
    pids = []
    for p in persos:
        pids.append(p.id)
    for p in pids:
        deleteperso(p)
    db.delete("detercellulis",where="cellulis='"+str(cid)+"' AND determination='"+str(did)+"'")

#Determination key for idy
def clefromid(idy):
    deters = db.select("determinations",where="id='"+str(idy)+"'")
    for d in deters:
        return d.cle
    return "Trou noir"

#Name of cellulis
def nomfromid(idy):
    cellulis = db.select("cellulis",where="id='"+str(idy)+"'")
    for c in cellulis:
        return c.nom
    return "Inconnu"

#Bind cellulis to determination
def bindcellulis(did,login):
    cid = None
    users = db.select("cellulis",where="login='"+login+"'")
    for u in users:
        cid = u.id
    if not cid:
        return False
    db.insert("detercellulis",cellulis=cid,determination=did)
    return True


#Character management
######################

    
def deleteperso(pid):
    perso = db.select("personnages",where="id='"+str(pid)+"'")
    for p in perso:
        pclass = p.rune
        if pclass == "renaissance":
            rmodel.deleteperso(pid)
    db.delete("personnages",where="id='"+str(pid)+"'")
    
#Check if user has access to character pid
def checkaccesspersonnage(user,pid,did):
    persos = db.select("personnages",where="id='"+str(pid)+"' AND cellulis='"+str(user)+"'")
    for p in persos:
        if p.id == int(pid):
            return "private"
    if persoindetermination(pid,did):
        return "public"
    return "no"

#Check if character pid is in determination did
def persoindetermination(pid,did):
    persos = db.select("personnages",where="id='"+str(pid)+"' AND determination='"+str(did)+"'")
    for p in persos:
        return True
    return False

#Get all characters of cellulis cid
def getpersonnages(cid):
    persos = db.select("personnages",where="cellulis='"+str(cid)+"'")
    return [x for x in persos]

def gettimestamp(pid):
    perso = db.select("personnages",where="id='"+str(pid)+"'")
    for p in perso:
        return int(p.updtime)
    return None

def updatetimestamp(pid):
    db.update("personnages",where="id='"+str(pid)+"'",updtime=int(time.time()))


#get rune of a character
def getrune(pid):
    perso = db.select("personnages",where="id='"+str(pid)+"'")
    for p in perso:
        return p.rune
    return None


def cellulistree(did):
    cellulis = []
    cellulisid = db.select("detercellulis",where="determination='"+str(did)+"'")
    for celid in cellulisid:
        cs = db.select("cellulis",where="id='"+str(celid.cellulis)+"'")
        for c in cs:
            cellulis.append(c)
    for cel in cellulis:
        cel.personnages = []
        persos = db.select("personnages",where="cellulis='"+str(cel.id)+"' AND determination='"+str(did)+"'")
        for p in persos:
            cel.personnages.append(p)
    return cellulis


def cellulistreeofrune(did,rune):
    cellulis = []
    cellulisid = db.select("detercellulis",where="determination='"+str(did)+"'")
    for celid in cellulisid:
        cs = db.select("cellulis",where="id='"+str(celid.cellulis)+"'")
        for c in cs:
            cellulis.append(c)
    for cel in cellulis:
        cel.personnages = []
        persos = db.select("personnages",where="cellulis='"+str(cel.id)+"' AND rune='"+str(rune)+"' AND determination='"+str(did)+"'")
        for p in persos:
            cel.personnages.append(p)
    return cellulis


concurrentlock = None

#Concurrency management
def createlock():
    global concurrentlock
    concurrentlock = os.path.join(tempfile.mkdtemp(suffix="cosmorebirth"),"lockfile")

def destroylock():
    os.remove(concurrentlock)
    os.rmdir(os.path.dirname(concurrentlock))
    
def acquirelock():
    while os.path.isfile(concurrentlock):
        time.sleep(0.1)
    open(concurrentlock,"w").close()

def loselock():
    os.remove(concurrentlock)
