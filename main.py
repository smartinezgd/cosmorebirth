#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# This file is part of Comso & Rebirth, a web application for Sens
# Hexalogie, role-playing game by Romaric Birand.

# Cosmo & Rebirth is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
 
# Comso & Rebirth is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details (COPYING.txt).
 
# See CREDITS.txt for credits of authorship
 
import web
import view
from view import render
import model
import time
import smtplib
from email.mime.text import MIMEText
import re
import sensconfig
import renaissance
import renaissance.rmodel
from random import randint

#import account.code as acode

urls = (
    #General
    "/","account.acode.index",
    "/logout","account.acode.logout",
    "/neant","account.acode.neant",
    "/account","account.acode.account",
    "/forget","account.acode.forget",
    "/pannel","pannel",
    "/creation","creation",
    "/neant","neant",
    "/deterneant","deterneant",
    "/cellulis","cellulis",
    "/pannel2","pannel2",
    #Renaissance
    "/export","renaissance.rcode.export",
    "/recensembler","renaissance.rcode.recensemble",
    "/recsensr","renaissance.rcode.recsens",
    "/reccellulisr","renaissance.rcode.reccellulis",
    "/reccellulisrm","renaissance.rcode.modalreccellulis",
)

app = web.application(urls,globals())

if not web.config.get('session'):
    init={'user':None,'type':"cellulis",'determination':None,'personnage':None,'lastload':0,'rune':None}
    session = web.session.Session(app, web.session.DiskStore('sessions'), initializer=init)
    web.config.session = session
else:
    session = web.config.session

def session_hook():
        web.ctx.session = session

app.add_processor(web.loadhook(session_hook))

model.createlock()

    
class pannel:
    def GET(self):
        user = session.user
        session.personnage = None
        session.determination = None
        session.type = "cellulis"
        if user:
            return render.base(view.pannel(user))
        else:
            raise web.seeother('/')

class creation:
    form = web.form.Form(
        web.form.Textbox('login1', description="Identifiant du premier cellulis :"),
        web.form.Textbox('login2', description="Identifiant du second cellulis :"),
        web.form.Textbox('login3', description="Identifiant du troisième cellulis :"),
        web.form.Textbox('login4', description="Identifiant du quatrième cellulis :"),
        web.form.Textbox('login5', description="Identifiant du cinquième cellulis :"),
    )
    def GET(self):
        form = self.form()
        return render.base(view.creation(form,False))

    def POST(self):
        form = self.form()
        form.validates()
        cellulis=[]
        for i in range(5):
            nom = form.inputs[i].value
            if nom:
                cellulis.append(nom)
        did = model.newdetermination(cellulis,session.user)
        if did:
            session.determination = did
            session.type = "sens"
            raise web.seeother('/pannel')
        #Aucune cellulis n'a pu être selectionné
        return render.base(view.creation(form,True))


class deterneant:
    def GET(self):
        raise web.seeother('/pannel')
    def POST(self):
        did = session.determination
        user = session.user
        typ = session.type
        if typ != "sens" or not model.checkaccessdetermination(user,"sens",did):
            raise web.seeother('/pannel')
        inp = web.input(suppr1=False,suppr2=False,suppr3=False)
        if inp.suppr1 and inp.suppr2 and inp.suppr3:
            model.enddetermination(did)
        raise web.seeother("/pannel")


class cellulis:
    def GET(self):
        did = session.determination
        user = session.user
        typ = session.type
        if typ != "sens" or not model.checkaccessdetermination(user,"sens",did):
            raise web.seeother('/pannel')
        return render.base(view.cellulis(did))        
    def POST(self):
        did = session.determination
        user = session.user
        typ = session.type
        if typ != "sens" or not model.checkaccessdetermination(user,"sens",did):
            raise web.seeother('/pannel')
        inp = web.input()
        #Parse the data
        tree = model.cellulistree(did)
        for cel in tree:
            if hasattr(inp,"suppr1"+str(cel.id)) and hasattr(inp,"suppr2"+str(cel.id)):
                if inp["suppr1"+str(cel.id)] and inp["suppr2"+str(cel.id)]:
                    #Remove cellulis from determination
                    model.unbindcellulis(cel.id,did)
            for perso in cel.personnages:
                if hasattr(inp,"suppr1"+str(perso.id)) and hasattr(inp,"suppr2"+str(perso.id)):
                    if inp["suppr1"+str(perso.id)] and inp["suppr2"+str(perso.id)]:
                        #delete perso
                        model.deleteperso(perso.id)
            if hasattr(inp,"new"+str(cel.id)):
                if inp["new"+str(cel.id)] == "bugrr":
                    renaissance.rmodel.newbugrebirth(cel.id,did)
                if inp["new"+str(cel.id)] == "bugdr":
                    renaissance.rmodel.newbugdeath(cel.id,did)
                if inp["new"+str(cel.id)] == "fragiler":
                    renaissance.rmodel.newfragile(cel.id,did)
                if inp["new"+str(cel.id)] == "quadrillar":
                    renaissance.rmodel.newquadrilla(cel.id,did)
        if hasattr(inp,"nomcel"):
            if inp.nomcel:
                #bind cellulis
                model.bindcellulis(did,inp.nomcel)
        raise web.seeother("/pannel2")


class pannel2:
    def GET(self):
        inp = web.input(id=None,type=None,perso=None,rune=None,timeout="0")
        #Load parameters from input
        did = inp.id
        if not did:
            if not session.determination:
                raise web.seeother('/pannel')
            did = session.determination
        else:
            session.determination = did
        typ = inp.type
        if not typ:
            if not session.type:
                session.type = "cellulis"
            typ = session.type
        session.type = typ
        pid = inp.perso
        if not pid:
            if not session.personnage:
                session.personnage = None
            pid = session.personnage
        session.personnage = pid
        rune = inp.rune
        if not rune:
            if not session.rune:
                session.rune = "renaissance"
            rune = session.rune
        session.rune = rune
        user = session.user
        timeout=(inp.timeout=="1")
        #Check access rights
        if not model.checkaccessdetermination(user,typ,did):
            raise web.seeother('/pannel')
        access = "no"
        if pid:
            if typ == "cellulis":
                rune = None
                access = model.checkaccesspersonnage(user,pid,did)
                if access != "no":
                    session.lastload = model.gettimestamp(pid)
                    rune = model.getrune(pid)
                    if not rune:
                        session.personnage = None
                        pid = None
                        session.rune = None
                    else:
                        session.rune = rune
                else:
                    session.personnage = None
                    pid = None
                    session.rune = None
            if typ == "sens":
                if "P" in pid:
                    lpid = pid.split("P")
                    session.lastload=""
                    for p in lpid:
                        if p != "":
                            if model.persoindetermination(int(p),did):
                                session.lastload += str(model.gettimestamp(int(p)))+"P"
                            else:
                                session.personnage = None
                                session.lastload = 0
                                pid = None
                                break
                else:
                    p = int(pid)
                    if model.persoindetermination(p,did):
                        session.lastload = model.gettimestamp(p)
                    else:
                        session.personnage = None
                        session.lastload = 0
                        pid = None
        #Render page
        x = randint(0,10)
        return render.base(view.pannel2(user,typ,did,pid,access,rune,timeout),x)
    
    def POST(self):
        did = session.determination
        user = session.user
        typ = session.type
        if typ != "sens" or not model.checkaccessdetermination(user,"sens",did):
            raise web.seeother('/pannel')
        inp = web.input(rune=None)
        rune = inp.rune
        if not rune:
            if not session.rune:
                session.rune = "renaissance"
            rune = session.rune
        session.rune = rune
        #field : id du perso
        cellulistree = model.cellulistreeofrune(did,rune)
        persofield = ""
        for cellulis in cellulistree:
            if hasattr(inp,str(cellulis.login)):
                for perso in cellulis.personnages:
                    if str(inp[str(cellulis.login)]) == str(perso.id):
                        persofield+=str(perso.id)+"P"
                        break
        print(persofield)
        session.personnage = persofield
        raise web.seeother("/pannel2")






    
application = app.wsgifunc()



if __name__ == "__main__":

    app.run()
