Cosmo & Rebirth
===============

Cosmos & Rebirth est une application web de gestion des personnages
pour le jeu de rôles _Sens_ de Romaric Briand.

License
=======

Comso & Rebirth est un logiciel libre publié sous license GPLv3 (voir
LICENSE.txt pour plus de détails). Vous êtes libre de le copier, de le
distribuer, de le modifier à votre guise selon les termes de la
license GPLv3. Le logiciel vous est livré en tant que tel et sans
aucune garantie.


Installation et configuration (debian Jessie)
=============================================

Copiez tous les fichiers dans un repertoire de votre choix. Veillez à
donner les droits d'accès à l'utilisateur __www-data__.

Dépendances
-----------

Cosmo & Rebirth requière Python2 et la bibliothèque __webpy__. La base
de donnée par défaut est __sqlite3__. Le présent guide d'installation
utilise __nginx__ et __uwsgi__ (muni de son plugin python), mais libre
à vous d'utiliser une autre plate-forme.

Configurer le serveur mail pour la récupération de mots de passe
----------------------------------------------------------------

Par défaut cette fonctionnalité est désactivée. Ce qui veut dire que
tout utilisateur ayant oublié son mot de passe ne pourra pas le
récupérer (via l'application).

Pour activer la récupération d'e-mail, vous devez avoir accès à un
serveur SMTP qui acceptera de transmettre les messages de
l'application. Dans la suite de ce paragraphe, on supposera que vous
avez accès à un tel serveur et que son adresse est `mon.serveur.fr`.

Ouvrez le fichier _sensconfig.py_. Remarquez que quatre variables y
sont définies.

1. __enablemail__ indique si la fonctionnalité de récupération des
mots de passe par e-mail est activée. Changez cette variable à `True`.

2. __messageTemplate__ est le modèle d'e-mail qui sera envoyé à chaque
demande de récupération de mot de passe. Vous n'êtes pas obligé de le
modifier.

3. __fromField__ définit l'adresse expéditrice des e-mails de
récupération. Remplacez _HOSTNAME_ par _mon.serveur.fr_.

4. __smtpServeur__ est l'adresse de votre serveur SMTP. Changez cette
   variable pour qu'elle vaille `"mon.serveur.fr"` 

Voilà, vous avez activé la récupération de mot de passe par e-mail.

Créer la base de données
------------------------

Dans le dossier où vous avez copié les fichiers, tappez les commandes
suivantes :

    sqlite3 cosmo.sqlite

Vous entrerez dans le prompt de commande de sqlite, tappez :

    .read script.sql

Puis quittez sqlite en tappant `Ctrl+D`.

Ensuite, pensez à vous assurer que l'utilisateur __www-data__ peut
lire et écrire dans ce fichier.

Configurer uwsgi
----------------

Installez __uwsgi__ avec le plugin Python (paquet
__uwsgi-plugin-python__) puis éditez sa configuration : dans le
dossier `/etc/uwsgi/apps-enabled/`, écrivez ces lignes dans le fichier
`vhosts.ini`:

    [uwsgi]
    id = www-data
    uid = www-data
    vhost = true
    logdate
    socket = /tmp/uwsgi_vhosts.sock
    master = true
    logto = /var/log/uwsgi/vhost.log
    plugins = python
    processes = 1
    harakiri = 20
    limit-as = 128
    memory-report
    no-orphans

Puis activez uwsgi et lancez-le :

    systemctl enbale uwsgi
    systemctl start uwsgi


Configurer nginx
----------------

Dans le dossier `/etc/nginx/sites-enabled` vous devriez avoir un
fichier __default__ (s'il n'existe pas, créez-le). Vous allez définir
un nouveau serveur qui va utiliser uwsgi pour servir l'application.

Recopiez ces lignes en remplaçant _/chemin/vers/sens_ par le chemin
vers le dossier où vous avez copié les fichiers.

    server {
        listen 443 ssl;
        listen [::]:443 ssl;

        ssl_certificate CERTIFICAT_SSL;
         ssl_certificate_key CLE_SSL;
    
        server_name VOTRE SERVEUR;
        location / {
                include uwsgi_params;
                uwsgi_pass unix:///tmp/uwsgi_vhosts.sock;
                uwsgi_param UWSGI_CHDIR /chemin/vers/sens;
                uwsgi_param UWSGI_PYHOME /chemin/vers/sens;
                uwsgi_param UWSGI_SCRIPT code;
        }
        location /static/ {
                root /chemin/vers/sens;
                if (-f $request_filename) {
                        rewrite ^/static/(.*)$  /static/$1 break;
                }
        }

    }


Puis activez et lancez nginx:

    systemctl enable nginx
    systemctl start nginx